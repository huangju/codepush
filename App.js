/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import codePush from "react-native-code-push";
const codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL };

export default class App extends Component {

  componentDidMount() {
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE,
      mandatoryInstallMode: codePush.InstallMode.IMMEDIATE,
      //deployment是刚才生成的Production的key
      //用Platform判断下平台
      deploymentKey: Platform.OS === 'ios' ? 'nD93HCWVCaLfftzY0P2Jp3DQnygJ171dec58-5ea7-4b60-8430-4e3a6d3995b0' : 'd5eKJHS8fGHjt7zHRpgl7-Oh3-sr171dec58-5ea7-4b60-8430-4e3a6d3995b0',
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Hello World</Text>
        <Text>手工</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
